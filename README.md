# API Code Challenge - System Overview

This document gives a brief overview of the system implemented, inline with the requirements specifications.

For installation instructions, please refer directly to the section [Running and using the software](#sinstallation).
 
## Code Structure
The following is a high-level overview of the codebase structure:

    .
    ├── .idea                                       # Definition of the IntelliJ Idea  workspace
    ├── dist                                        # Contains the latest executable JAR file
    ├── src/main/java/com/codechallenge/api         # The implementation package
    │   ├── config                                  # Security, Swagger and generic application configuration classes
    │   ├── error                                   # Error Controller and Problem Detail definitions
    │   ├── home                                    # A package containing a simple home page controller
    │   ├── mobileno                                # A package implementing the functionality related to mobile number management
    │   ├── security                                # A package implementing security logic, based on JWT tokens
    │   └── user                                    # A package implementing basic user management, required by security logic
    └── src/test/java/com/codechallenge/api         # Test classes, same structure as the main package
    

## <span id="sinstallation">Running and using the software</span>
The latest version of the software can be executed by:
1. Navigating to the `/dist` directory
2. Executing `java -jar api-0.0.1-SNAPSHOT.jar`

A simple helper runner, `run.bat`, also located in the `/dist` directory, can also be used.

By default, the API will become accessible on `http://localhost:8080`.

The API can be exercised directly via the Swagger UI, which by default loads on the endpoint `/swagger-ui/`.

The following endpoints are exposed:
1. GET `/user/login`, an endpoint that allows an `admin` user to authenticate.  
2. GET `/mobile-no/all`, an endpoint that returns the list of mobile numbers stored. Returns an empty list if none are found.
3. GET `/mobile-no/{id}`, an endpoint that returns the details of a specific mobile number. Returns a 404 response if the mobile number does not exist.
4. GET `/mobile-no/search/{msisdn}`, an endpoint that searches for mobile numbers by performing a partial match on the MSISDN property. Returns an empty list if no matching entries are found.
5. POST `/mobile-no`, an endpoint that creates a new mobile number. Returns a 406 response if a mobile number with the same MSISDN is already stored. Returns a 500 error code if the data does not validate (e.g. the MSISDN is in an invalid format)
6. DELETE `/mobile-no/{id}`, an endpoint that deletes a specific mobile number. Returns a 404 response if the mobile number does not exist.
7. PUT `/mobile-no/changeOwner/{id}`, an endpoint that modifies the owner of a specific mobile number. Returns a 404 response if the mobile number does not exist.
8. PUT `/mobile-no/changeUser/{id}`, an endpoint that modifies the user of a specific mobile number. Returns a 404 response if the mobile number does not exist.
9. PUT `/mobile-no/changeServiceType/{id}`, an endpoint that modifies the service type of a specific mobile number. Returns a 404 response if the mobile number does not exist.

All the endpoints except `/user/login` require authentication via a JWT token, submitted in the `Authorization` header of the HTTP request.

JWT tokens are by default configured to expire after 15 minutes.

Therefore, in order to use any of the endpoints under `/mobile-no`:
1. Retrieve a valid JWT token by calling the `/user/login` endpoint for credentials `admin/admin`, e.g.: `curl -X POST "http://localhost:8080/user/login" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"password\": \"admin\", \"userName\": \"admin\"}"`
2. Send the JWT token in the Authorisation header, with a `Bearer` prefix e.g.: `curl -X GET "http://localhost:8080/mobile-no/1" -H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOnsiYXV0aG9yaXR5IjoiZGVtby11c2VyIn0sImlhdCI6MTYzMjkxNjY1MCwiZXhwIjoxNjMyOTE4MTUwfQ.dxIpLyVCOOw6hALVx98h7qiaCdS8n26SW4gZzOBMHniQB0wJ0zcQukB_1KGYEDgdysMwMaNlBsObGU9pC7yC4Q"`

When using Swagger UI, step [2] can be automated by setting the header value as below via the "Authorize" button:
![img.png](doc/scr_auth.png)



## <span id="sapispecs">API Specifications</span>
Further to the details given in Section [Running and using the software](#sinstallation), the Swagger OAS 3 API definitions can be accessed at the endpoint `/v3/api-docs`.
 

## <span id="sinstallation">Implementation Approach</span>
This section summarises a few details about the approach taken to implement the requirements.

### REST Protocol
REST principles were followed in the implementation of the `mobile-no` API, in particular through the use of proper HTTP verbs:
1. `GET` is used when data is retrieved
2. `POST` is used when new record are created
3. `PUT` is used when records are updated

### Security Protocol
An authentication mechanism based on JWT tokens was chosen for this implementation. This is in-line with best practices but also conforms to the requirement that the system would be running in load-balanced, active-active environment, where a basic (e.g. in-memory) session-based mechanism would fail.

Naturally other approaches (such as a session-based mechanism based on a distributed database) could also satisfy the requirement, but would need additional components to be installed (e.g. Redis or MemCache).


### Data Modeling
The API is kept as extensible as possible through the use of Data Transfer Objects (DTOs).

Where relevant, the inputs of API endpoints are modeled as DTOs (e.g. the input of the `/mobile-no/changeOwner/{id}`) in order to allow easier extensibility should more data be required in the input eventually.

DTOs are also kept separate, as much as possible, from Data Access Objects (DAOs) in order to keep the internal representation of business objects as de-coupled as possible from the API specifications.


### Error Responses
The implementation follows the Problem Details specification (IETF RFC 7807) to report errors in a machine-readable format.

For example, calling the API `/mobile-no/{id}` with an identifier that does not exist returns a 404 error code, with the below response:
```json
{
  "type": "https://httpstatuses.com/404",
  "title": "Not found",
  "status": 404,
  "detail": "Entity not found"
}
```

### Error Management
The implementation aims to incorporate some functional aspects, in particular through the use of Monads in the return values of some methods. A `Try` monad from the package `io.vavr.vavr` is used to encapsulate error messages where necessary.

### Test Coverage
A unit test suite is included with the implementation, such that every package exceeds the expected 80% method and line coverage, as below:
![img.png](doc/coverage.png)

### Data Repository
For the purpose of this implementation, the H2 in-memory database is used. Naturally this is for prototyping purposes only, however since the implementation uses JpaRepository, it is envisaged that switching to a production-ready database (e.g. Postgresql) can be done through configuration only.

### User Authentication
For the purpose of this implementation, the `/user/login` endpoint accepts only one user, `admin` with password `admin`, since user management was not part of the scope. 

It is however envisaged that extending this to proper user management functionality, with user credentials stored in the database, would necessitate only changes in the `user` package, and specifically in the `UserRepository` implementation, thus keeping the extension transparent from the rest of the security features.    

### Data set size 
This implementation does not assume a large data set size.

Specifically, the endpoints `/mobile-no/all` and `/mobile-no/search`/ could fail if the size of the dataset grows to a level where the response is too big. However, it is envisaged that these two APIs could be extended by a pagination or cursor-based mechanism, allowing clients to retrieve large data sets.

