package com.codechallenge.api.mobileno;

import com.codechallenge.api.error.problemdetail.DuplicateEntity;
import com.codechallenge.api.error.problemdetail.EntityError;
import com.codechallenge.api.error.problemdetail.EntityNotFound;
import com.codechallenge.api.mobileno.dao.MobileServiceType;
import com.codechallenge.api.mobileno.dto.CreateMobileNoRequest;
import com.codechallenge.api.mobileno.dto.UpdateOwnerRequest;
import com.codechallenge.api.mobileno.dto.UpdateServiceTypeRequest;
import com.codechallenge.api.mobileno.dto.UpdateUserRequest;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class MobileNoControllerTests {


    private transient final MobileNoController mobileNoController;
    private static final List<String> seedMobileNos = List.of("21212121","21212122");

    @BeforeEach
    public void seedDatabase() {
        var mobileNos = mobileNoController.getAll();
        Objects.requireNonNull(mobileNos.getBody()).forEach(mobileNo->mobileNoController.delete(mobileNo.getId()));

        seedMobileNos.forEach(seedMobileNo ->
                mobileNoController.create(new CreateMobileNoRequest(seedMobileNo, 1, 1, MobileServiceType.MOBILE_POSTPAID, new Date()))
        );

    }


    @Autowired
    public MobileNoControllerTests(final MobileNoController mobileNoController) {
        this.mobileNoController = mobileNoController;
    }


    @Test
        /* default */ void create() {

        var startDate = new Date();
        var data = new CreateMobileNoRequest();
        data.setMsisdn("2222222");
        data.setCustomerIdOwner(1);
        data.setCustomerIdUser(1);
        data.setServiceStartDate(startDate);
        data.setServiceType(MobileServiceType.MOBILE_POSTPAID);

        assertThat(data.getMsisdn()).isEqualTo("2222222");
        assertThat(data.getCustomerIdOwner()).isEqualTo(1);
        assertThat(data.getCustomerIdUser()).isEqualTo(1);
        assertThat(data.getServiceStartDate()).isEqualTo(startDate);
        assertThat(data.getServiceType()).isEqualTo(MobileServiceType.MOBILE_POSTPAID);

        var res = mobileNoController.create(data);
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).getId()).isGreaterThanOrEqualTo(1);

    }


    @Test
        /* default */ void createFailDuplicate() {

        var startDate = new Date();
        var data = new CreateMobileNoRequest(seedMobileNos.get(0),1,1, MobileServiceType.MOBILE_POSTPAID,new Date());
        assertThatThrownBy(()->mobileNoController.create(data)).isInstanceOf(DuplicateEntity.class);

    }


    @Test
        /* default */ void createFailInvalidPhone() {

        var startDate = new Date();
        var data = new CreateMobileNoRequest("9",1,1, MobileServiceType.MOBILE_POSTPAID,new Date());
        assertThatThrownBy(()->mobileNoController.create(data)).isInstanceOf(EntityError.class);

    }


    @Test
        /* default */ void get() {
        var id = Objects.requireNonNull(mobileNoController.getAll().getBody()).get(0).getId();
        var res = mobileNoController.get(id);
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).getMsisdn()).isEqualTo(seedMobileNos.get(0));

    }


    @Test
        /* default */ void getFail() {
        assertThatThrownBy(()-> mobileNoController.get(100)).isInstanceOf(EntityNotFound.class);

    }

    @Test
    /* default */ void getAll() {
        var res = mobileNoController.getAll();
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).size()).isGreaterThanOrEqualTo(seedMobileNos.size());
    }


    @Test
        /* default */ void deleteSuccess() {
        var mobileNos = mobileNoController.getAll().getBody();
        assertThat(mobileNos).isNotNull();
        mobileNos.forEach(mobileNo->mobileNoController.delete(mobileNo.getId()));

        var res = mobileNoController.getAll();

        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).size()).isEqualTo(0);

    }


    @Test
    void deleteFail() {
        assertThatThrownBy(()->
                mobileNoController.delete(1000)).isInstanceOf(EntityNotFound.class);
    }


    @Test
        /* default */ void search() {
        var res = mobileNoController.search(seedMobileNos.get(0).substring(0,3));
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).size()).isGreaterThanOrEqualTo(1);

    }


    @Test
        /* default */ void searchFail() {
        var res = mobileNoController.search("999");
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody()).size()).isEqualTo(0);

    }



    @Test
    void updateOwnerFail() {
         assertThatThrownBy(()->
                mobileNoController.updateOwner(1000,new UpdateOwnerRequest(1))).isInstanceOf(EntityNotFound.class);
    }

    @Test
    void updateOwnerSuccess() {
            var newOwnerId=100;
        var id = Objects.requireNonNull(mobileNoController.getAll().getBody()).get(0).getId();
        var res = mobileNoController.updateOwner(id,new UpdateOwnerRequest(newOwnerId));
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody())).isTrue();

        var mobileNo = mobileNoController.get(id).getBody();
        assertThat(mobileNo).isNotNull();
        assertThat(mobileNo.getCustomerIdOwner()).isEqualTo(newOwnerId);
    }

    @Test
    void updateUserFail() {
        assertThatThrownBy(()->
                mobileNoController.updateUser(1000,new UpdateUserRequest(1))).isInstanceOf(EntityNotFound.class);
    }

    @Test
    void updateUserSuccess() {
        var newUserId=100;
        var id = Objects.requireNonNull(mobileNoController.getAll().getBody()).get(0).getId();
        var res = mobileNoController.updateUser(id,new UpdateUserRequest(newUserId));
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody())).isTrue();

        var mobileNo = mobileNoController.get(id).getBody();
        assertThat(mobileNo).isNotNull();
        assertThat(mobileNo.getCustomerIdUser()).isEqualTo(newUserId);
    }

    @Test
    void updateServiceTypeFail() {
        assertThatThrownBy(()->
                mobileNoController.updateServiceType(1000,new UpdateServiceTypeRequest(MobileServiceType.MOBILE_POSTPAID))).isInstanceOf(EntityNotFound.class);
    }

    @Test
    void updateServiceTypeSuccess() {
        var newUserId=100;
        var id = Objects.requireNonNull(mobileNoController.getAll().getBody()).get(0).getId();
        var res = mobileNoController.updateServiceType(id,new UpdateServiceTypeRequest(MobileServiceType.MOBILE_POSTPAID));
        assertThat(res).isNotNull();
        assertThat(res.getBody()).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(Objects.requireNonNull(res.getBody())).isTrue();

        var mobileNo = mobileNoController.get(id).getBody();
        assertThat(mobileNo).isNotNull();
        assertThat(mobileNo.getServiceType()).isEqualTo(MobileServiceType.MOBILE_POSTPAID);
        var serviceStartDate = mobileNo.getServiceStartDate();
        mobileNoController.updateServiceType(id,new UpdateServiceTypeRequest(MobileServiceType.MOBILE_PREPAID));

        mobileNo = mobileNoController.get(id).getBody();
        assertThat(mobileNo).isNotNull();
        assertThat(mobileNo.getServiceType()).isEqualTo(MobileServiceType.MOBILE_PREPAID);
        assertThat(mobileNo.getServiceStartDate()).isEqualTo(serviceStartDate);
    }


}
