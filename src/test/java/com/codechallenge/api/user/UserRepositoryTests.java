package com.codechallenge.api.user;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserRepositoryTests {


    private transient final UserRepository userRepository;

    @Autowired
    public UserRepositoryTests(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Test
    /* default */ void loginSuccess() {
        var res = userRepository.findByUsernameAndPassword("admin","admin");
        assertThat(res).isNotNull();
        assertThat(res.getUsername()).isEqualTo("admin");
        assertThat(res.getPassword()).isNull();
        assertThat(res.getId()).isGreaterThanOrEqualTo(0);
        assertThat((long) res.getClaims().size()).isGreaterThan(0);
    }

    @Test
        /* default */ void loginFail() {
        var res = userRepository.findByUsernameAndPassword("invalid","invalid");
        assertThat(res).isNull();

    }

}
