package com.codechallenge.api.user;

import com.codechallenge.api.user.dao.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserDaoTests {


    @Autowired
    public UserDaoTests() {

    }

    @Test
    /* default */ void creationSuccess() {
        var user = new User();
        user.setId(1);
        user.setUsername("admin");
        user.setClaims(new ArrayList<>(List.of("admin")));

        assertThat(user.getId()).isEqualTo(1);
        assertThat(user.getUsername()).isEqualTo("admin");
        assertThat(user.getClaims().get(0)).isEqualTo("admin");

    }


}
