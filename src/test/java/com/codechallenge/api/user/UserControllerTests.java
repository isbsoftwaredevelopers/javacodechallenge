package com.codechallenge.api.user;

import com.codechallenge.api.error.problemdetail.EntityNotFound;
import com.codechallenge.api.user.dto.LoginRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
class UserControllerTests {


    private transient final UserController userController;

    @Autowired
    public UserControllerTests(final UserController userController) {
        this.userController = userController;
    }

    @Test
    /* default */ void loginSuccess() {
        var res = userController.login(null,new LoginRequest("admin","admin"));
        assertThat(res.getStatusCode() == HttpStatus.OK).isTrue();

    }

    @Test
        /* default */ void loginFail() {

        assertThatThrownBy(()->
            userController.login(null, new LoginRequest("invalid", "invalid"))
        ).isInstanceOf(EntityNotFound.class);


    }



}
