package com.codechallenge.api.user;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserServiceTests {


    private transient final UserService userService;

    @Autowired
    public UserServiceTests(UserService userService) {
        this.userService = userService;
    }

    @Test
    /* default */ void loginSuccess() {
        var res = userService.login("admin","admin");
        assertThat(res.isPresent()).isTrue();
        assertThat(res.get().getJwtToken().length()).isGreaterThan(10);

    }

    @Test
        /* default */ void loginFail() {
        var res = userService.login("invalid","invalid");
        assertThat(res.isPresent()).isFalse();

    }

}
