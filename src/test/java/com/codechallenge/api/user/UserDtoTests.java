package com.codechallenge.api.user;

import com.codechallenge.api.mobileno.dao.MobileServiceType;
import com.codechallenge.api.mobileno.dto.UpdateOwnerRequest;
import com.codechallenge.api.mobileno.dto.UpdateServiceTypeRequest;
import com.codechallenge.api.mobileno.dto.UpdateUserRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserDtoTests {


    @Autowired
    public UserDtoTests() {

    }

    @Test
    /* default */ void creationUpdateOwnerRequestSuccess() {
        var user = new UpdateOwnerRequest();
        user.setUserId(1);
        assertThat(user.getUserId()).isEqualTo(1);

    }

    @Test
        /* default */ void creationUpdateUserRequestSuccess() {
        var user = new UpdateUserRequest();
        user.setUserId(1);
        assertThat(user.getUserId()).isEqualTo(1);

    }

    @Test
        /* default */ void creationUpdateServiceTypeSuccess() {
        var user = new UpdateServiceTypeRequest();
        user.setServiceType(MobileServiceType.MOBILE_POSTPAID);
        assertThat(user.getServiceType()).isEqualTo(MobileServiceType.MOBILE_POSTPAID);

    }

}
