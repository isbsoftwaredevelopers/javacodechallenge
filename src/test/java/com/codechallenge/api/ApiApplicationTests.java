package com.codechallenge.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ApiApplicationTests {


    private transient final ApiApplication controller;

    @Autowired
    public ApiApplicationTests(ApiApplication controller) {
        this.controller = controller;
    }

    @Test
    /* default */ void contextLoads() {
        assertThat(this.controller).isNotNull();
        Assertions.assertDoesNotThrow(()->ApiApplication.main(new String[0]));

    }

}
