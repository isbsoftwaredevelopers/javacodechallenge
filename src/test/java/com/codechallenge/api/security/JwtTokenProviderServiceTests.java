package com.codechallenge.api.security;

import com.codechallenge.api.user.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
class JwtTokenProviderServiceTests {


    private transient final JwtTokenProviderService jwtTokenProviderService;

    @Autowired
    public JwtTokenProviderServiceTests(JwtTokenProviderService jwtTokenProviderService) {
        this.jwtTokenProviderService = jwtTokenProviderService;
    }

    @Test
    /* default */ void createTokenSuccess() {
        var res = jwtTokenProviderService.createToken("admin", List.of("admin"));
        assertThat(res).isNotNull();
        assertThat(res.length()).isGreaterThan(10);

    }

    @Test
        /* default */ void parseTokenSuccess() {

        var token = jwtTokenProviderService.createToken("admin", List.of("admin"));
        HttpServletRequest request = mock(HttpServletRequest.class);

        String authHeader = String.format("Bearer %s",token);
        when(request.getHeader("Authorization")).thenReturn(authHeader);
        var res = jwtTokenProviderService.parseToken(request);
        assertThat(res).isNotNull();
        assertThat(res.length()).isGreaterThan(10);

    }

    @Test
        /* default */ void parseTokenFail() {

        var token = jwtTokenProviderService.createToken("admin", List.of("admin"));
        HttpServletRequest request = mock(HttpServletRequest.class);

        String authHeader = String.format("%s",token);
        when(request.getHeader("Authorization")).thenReturn(authHeader);
        var res = jwtTokenProviderService.parseToken(request);
        assertThat(res).isNull();

    }


    @Test
        /* default */ void validateTokenSuccess() {

        var token = jwtTokenProviderService.createToken("admin", List.of("admin"));
        var res = jwtTokenProviderService.validateToken(token);
        assertThat(res).isTrue();

    }

        @Test
        /* default */ void validateTokenFail() {
            assertThatThrownBy(()->
            jwtTokenProviderService.validateToken("invalid token")).isInstanceOf(RuntimeException.class);


        }

        @Test
        /* default */ void validateUserAndGetAuthenticationSuccess() {
            var token = jwtTokenProviderService.createToken("admin", List.of("admin"));
            var res = jwtTokenProviderService.validateUserAndGetAuthentication(token);
            assertThat(res).isNotNull();
        }


}
