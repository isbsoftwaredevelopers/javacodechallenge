package com.codechallenge.api.config;

import com.codechallenge.api.home.HomeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AppConfigUtilsTests {

    @Autowired
    public AppConfigUtilsTests() {
    }

    @Test
    /* default */ void configSuccess() {

        assertThat(AppConfigUtils.JwtSecret).isNotNull();
        assertThat(AppConfigUtils.JwtTokenValidityMs).isGreaterThan(10000);
    }


}
