package com.codechallenge.api.home;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class HomeControllerTests {


    private transient final HomeController homeController;

    @Autowired
    public HomeControllerTests(final HomeController homeController) {
        this.homeController = homeController;
    }

    @Test
    /* default */ void homeSuccess() {

        var res = homeController.index();
        assertThat(res).isNotNull();
        assertThat(res).contains("swagger");

    }


}
