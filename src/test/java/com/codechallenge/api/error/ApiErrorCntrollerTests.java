package com.codechallenge.api.error;

import com.codechallenge.api.error.problemdetail.DuplicateEntity;
import com.codechallenge.api.error.problemdetail.EntityError;
import com.codechallenge.api.error.problemdetail.EntityNotFound;
import com.codechallenge.api.home.HomeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class ApiErrorCntrollerTests {

    private transient final ApiErrorController apiErrorController;

    @Autowired
    public ApiErrorCntrollerTests(final ApiErrorController apiErrorController) {
        this.apiErrorController = apiErrorController;
    }

    @Test
    /* default */ void handleProblemEntityNotFoundSuccess() {

        var res = apiErrorController.handleProblem(new EntityNotFound());
        assertThat(res).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

    }


    @Test
        /* default */ void handleProblemEntityErrorSuccess() {
        var res = apiErrorController.handleProblem(new EntityError(new RuntimeException("Error happened")));
        assertThat(res).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @Test
        /* default */ void handleProblemDuplicateEntitySuccess() {
        var res = apiErrorController.handleProblem(new DuplicateEntity());
        assertThat(res).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.NOT_ACCEPTABLE);

    }


    @Test
        /* default */ void handleExceptionSuccess() {
        var res = apiErrorController.handleException(new RuntimeException("Error happened"));
        assertThat(res).isNotNull();
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
