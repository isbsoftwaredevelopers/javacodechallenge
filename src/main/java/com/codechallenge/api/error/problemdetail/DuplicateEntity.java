package com.codechallenge.api.error.problemdetail;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * Follows the Problem Detail specification of the IETF RFC 7807
 * Returned when trying to create an entity (e.g. mobile number) which already exists according to validation rules
 */
public class DuplicateEntity extends AbstractThrowableProblem {

    private static final URI type = URI.create("https://httpstatuses.com/406");

    public DuplicateEntity() {
        super(
                type,
                "Already Exists",
                Status.NOT_ACCEPTABLE,
                "Entity already exists");
    }

}
