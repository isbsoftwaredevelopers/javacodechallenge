package com.codechallenge.api.error.problemdetail;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * Follows the Problem Detail specification of the IETF RFC 7807
 * Returned when trying to access an entity (e.g. mobile number) which is not found
 */
public class EntityNotFound extends AbstractThrowableProblem {

    private static final URI type = URI.create("https://httpstatuses.com/404");

    public EntityNotFound() {
        super(
                type,
                "Not found",
                Status.NOT_FOUND,
                "Entity not found");
    }

}
