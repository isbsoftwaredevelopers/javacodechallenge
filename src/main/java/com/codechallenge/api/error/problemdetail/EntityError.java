package com.codechallenge.api.error.problemdetail;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

import java.net.URI;

/**
 * Follows the Problem Detail specification of the IETF RFC 7807
 * Returned when trying to create an entity (e.g. mobile number) which already exists according to validation rules
 */
public class EntityError extends AbstractThrowableProblem {

    private static final URI type = URI.create("https://httpstatuses.com/500");

    public EntityError(final Throwable exception) {
        super(
                type,
                "Server Error",
                Status.INTERNAL_SERVER_ERROR,
                exception.getMessage());
    }

}
