package com.codechallenge.api.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Problem;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@ControllerAdvice
public class ApiErrorController {

    @ExceptionHandler(AbstractThrowableProblem.class)
    public ResponseEntity<Problem> handleProblem(final AbstractThrowableProblem problem) {
        return new ResponseEntity<>(problem,HttpStatus.valueOf(problem.getStatus().getStatusCode()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, String>> handleException(final Exception e)  {
        final var errorResponse = new ConcurrentHashMap<String,String>();
        errorResponse.put("message", e.getLocalizedMessage());
        errorResponse.put("status", HttpStatus.INTERNAL_SERVER_ERROR.toString());

        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
