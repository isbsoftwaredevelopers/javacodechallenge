package com.codechallenge.api.security;

import com.codechallenge.api.config.AppConfigUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * An injectable class that encapsulates functionality to create JWT tokens for the "admin" user
 */
@Service
public class JwtTokenProviderService implements IJwtTokenProviderService  {

    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProviderService.class);

    @Override
    public Authentication validateUserAndGetAuthentication(final String token) {
        return new UsernamePasswordAuthenticationToken("admin", null, null);
    }

    @Override
    public String createToken(final String userName, final List<String> claims) {
        final Claims tokenClaims = Jwts.claims().setSubject(userName);

        claims.stream()
                .map(claim->new SimpleGrantedAuthority("demo-user"))
                .forEach(claim->tokenClaims.put("auth",claim));


        final Date now = new Date();
        final Date tokenExpiryTime = new Date(now.getTime() + AppConfigUtils.JwtTokenValidityMs);

        return Jwts.builder()
                .setClaims(tokenClaims)
                .setIssuedAt(now)
                .setExpiration(tokenExpiryTime)
                .signWith(SignatureAlgorithm.HS512, AppConfigUtils.JwtSecret)
                .compact();
    }



    @Override
    public String parseToken(final HttpServletRequest req) {
        final String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    @Override
    public boolean validateToken(final String token) {
        try {
            Jwts.parser().setSigningKey(AppConfigUtils.JwtSecret).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException ex) {
            if(logger.isErrorEnabled()) {
                logger.error(String.format("Invalid token: %s", ex.getMessage()), ex);
            }
            throw new RuntimeException ("Invalid token",ex);
        }
    }
}
