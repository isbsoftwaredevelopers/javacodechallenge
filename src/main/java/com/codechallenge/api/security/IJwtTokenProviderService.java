package com.codechallenge.api.security;


import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Defines the specification for a provider of JWT tokens
 */
public interface IJwtTokenProviderService {

    String createToken(String username,List<String> claims);

    Authentication validateUserAndGetAuthentication(String token);
    String parseToken(HttpServletRequest req);
    boolean validateToken(String token);

}