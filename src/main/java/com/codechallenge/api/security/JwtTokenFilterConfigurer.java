package com.codechallenge.api.security;


import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final IJwtTokenProviderService jwtTokenProviderService;

    public JwtTokenFilterConfigurer(final IJwtTokenProviderService jwtTokenProviderService) {
        this.jwtTokenProviderService = jwtTokenProviderService;
    }

    @Override
    public void configure(final HttpSecurity http)  {
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProviderService), UsernamePasswordAuthenticationFilter.class);
    }

}