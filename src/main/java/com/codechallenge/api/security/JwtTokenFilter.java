package com.codechallenge.api.security;


import org.springframework.lang.NonNull;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends OncePerRequestFilter {

    private final IJwtTokenProviderService jwtTokenProviderService;


    public JwtTokenFilter(final IJwtTokenProviderService jwtTokenProviderService) {
        this.jwtTokenProviderService = jwtTokenProviderService;
    }




    @Override
    protected void doFilterInternal(@NonNull final HttpServletRequest httpServletRequest, @NonNull final HttpServletResponse httpServletResponse, @NonNull final FilterChain filterChain) throws ServletException, IOException {
        final String token = jwtTokenProviderService.parseToken(httpServletRequest);

        if (token != null && jwtTokenProviderService.validateToken(token)) {
            final var auth = jwtTokenProviderService.validateUserAndGetAuthentication(token);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}