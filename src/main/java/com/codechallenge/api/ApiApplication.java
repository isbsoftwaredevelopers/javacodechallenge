package com.codechallenge.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("PMD.ClassWithOnlyPrivateConstructorsShouldBeFinal")
@SpringBootApplication
public  class ApiApplication {


	public static void main(final String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
