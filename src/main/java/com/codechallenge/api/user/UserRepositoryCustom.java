package com.codechallenge.api.user;

import com.codechallenge.api.user.dao.User;

public interface UserRepositoryCustom {
    User findByUsernameAndPassword(String username, String password);
}
