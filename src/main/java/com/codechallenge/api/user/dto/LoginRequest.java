package com.codechallenge.api.user.dto;

public class LoginRequest {
    private final String userName;
    private final String password;

    public LoginRequest(final String userName,final String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }


    public String getUserName() {
        return userName;
    }

}
