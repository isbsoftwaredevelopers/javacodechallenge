package com.codechallenge.api.user.dto;

public class LoginResponse {
    private final String jwtToken;


    public LoginResponse(final String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
