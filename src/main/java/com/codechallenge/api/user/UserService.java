package com.codechallenge.api.user;

import com.codechallenge.api.security.IJwtTokenProviderService;
import com.codechallenge.api.user.dto.LoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements IUserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final IJwtTokenProviderService jwtTokenProviderService;

    public UserService(final UserRepository userRepository, final IJwtTokenProviderService jwtTokenProviderService) {
        this.userRepository = userRepository;
        this.jwtTokenProviderService = jwtTokenProviderService;
    }

    @Override
    public Optional<LoginResponse> login(final String username,final  String password) {
        final var user = userRepository.findByUsernameAndPassword(username,password);
        if(user!=null) {
            final var res  = new LoginResponse(jwtTokenProviderService.createToken(username,user.getClaims()));
            logger.info("Login successfully");
            return Optional.of(res);
        }

        if(logger.isErrorEnabled()) {
            logger.error(String.format("Invalid login for %s",username));
        }

        return Optional.empty();
    }
}
