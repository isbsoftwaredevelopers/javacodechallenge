package com.codechallenge.api.user;


import com.codechallenge.api.error.problemdetail.EntityNotFound;
import com.codechallenge.api.user.dto.LoginRequest;
import com.codechallenge.api.user.dto.LoginResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("user")
public class UserController {

    private final IUserService userService;

    UserController(final IUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(final HttpServletRequest requestHeader, final @RequestBody LoginRequest request) {
        final var loginResponse = userService.login(request.getUserName(), request.getPassword());
        if(loginResponse.isEmpty()){
            throw new EntityNotFound();
        }else{
            return new ResponseEntity<>(loginResponse.get(), HttpStatus.OK);
        }
    }

}