package com.codechallenge.api.user.dao;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

    private String username;

    private String password;

    @Id
    private Integer id;

    @ElementCollection
    private List<String> claims;

    public User(){
        //Default constructor
    }

    public User(final String username, final List<String> claims) {
        this.username = username;
        this.claims = claims;
        this.id=0;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public void setId(final Integer id) {
        this.id = id;
    }


    public Integer getId() {
        return id;
    }


    public List<String> getClaims() {
        return new ArrayList<>(claims);
    }

    public void setClaims(final List<String> claims) {
        this.claims = claims;
    }

    public String getPassword() {
        return password;
    }
}
