package com.codechallenge.api.user;

import com.codechallenge.api.user.dao.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>,UserRepositoryCustom {

    User findByUsernameAndPassword(String username, String password);


}
