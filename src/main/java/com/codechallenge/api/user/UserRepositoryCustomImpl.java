package com.codechallenge.api.user;

import com.codechallenge.api.user.dao.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @Override
    public User findByUsernameAndPassword(final String username,final  String password) {
        if ("admin".equals(username) && "admin".equals(password)) {
            return new User(username,new ArrayList<>( List.of("admin")));
        }
        return null;
    }
}
