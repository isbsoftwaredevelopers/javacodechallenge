package com.codechallenge.api.user;

import com.codechallenge.api.user.dto.LoginResponse;

import java.util.Optional;

public interface IUserService {
    Optional<LoginResponse> login(String username, String password);

}
