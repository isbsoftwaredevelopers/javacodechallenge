package com.codechallenge.api.home;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {



    @GetMapping("/")
    public String index() {
        return "Welcome to Code Challenge API. Review Swagger Docs at <a href=\"/swagger-ui/\">/swagger-ui/</a>";
    }

}