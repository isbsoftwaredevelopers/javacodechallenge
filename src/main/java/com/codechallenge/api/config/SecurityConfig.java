package com.codechallenge.api.config;

import com.codechallenge.api.security.IJwtTokenProviderService;
import com.codechallenge.api.security.JwtTokenFilterConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableWebSecurity
@EnableTransactionManagement
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final IJwtTokenProviderService jwtTokenProviderService;

    public SecurityConfig(final IJwtTokenProviderService jwtTokenProviderService) {
        this.jwtTokenProviderService = jwtTokenProviderService;
    }


    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        //-- Disable CSRF --//
        http.csrf().disable();

        //-- No session needed --//
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //-- All requests require authentication, unless defined further down --//
        http.authorizeRequests(req -> req.anyRequest().authenticated());

        //-- Use JWT for stateless authentication --//
        http.apply(new JwtTokenFilterConfigurer(jwtTokenProviderService));

    }

    @Override
    public void configure(final WebSecurity web)  {
        //--  Exclude Home Page, Login and Swagger docs from requiring authentication --//
        web.ignoring().antMatchers("/v2/api-docs")
                .antMatchers("/v3/api-docs")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/swagger-ui/")
                .antMatchers("/swagger-ui/**")
                .antMatchers("/swagger")
                .antMatchers("/user")
                .antMatchers("/user/**")
                .antMatchers("/");

    }


}