package com.codechallenge.api.config;

/**
 * Global configuration parameters
 */
public class AppConfigUtils {
    /**
     * 15 minutes validity
     */
    public final static long JwtTokenValidityMs = 1_500_000;


    /**
     * Secret used for JWT generation
     */
    public final static String JwtSecret = "49bbbc5c-d9aa-4514-a3e1-ccf8c7bd6dd4";

}
