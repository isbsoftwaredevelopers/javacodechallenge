package com.codechallenge.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * Configuration for Swagger documentation
 */
@Configuration
@EnableSwagger2
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket swaggerConfigDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .securityContexts(List.of(buildSecurityContext()))
                .securitySchemes(List.of(generateApiKey()))
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * Generates an API key that requires a JWT token to be passed in the Authorization HTTP header --//
     */
    private ApiKey generateApiKey() {
        return new ApiKey("jwttoken", "Authorization", "header");
    }


    /**
     * Builds the security context, based on the JWT token configuration
     */
    private SecurityContext buildSecurityContext() {
        return SecurityContext.builder().securityReferences(jwtAuth()).build();
    }

    /**
     * Setup the default JWT-based authentication
     */
    private List<SecurityReference> jwtAuth() {
        final var authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("jwttoken", authorizationScopes));
    }

}
