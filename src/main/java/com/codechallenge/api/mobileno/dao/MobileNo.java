package com.codechallenge.api.mobileno.dao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class MobileNo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String msisdn;
    private Integer customerIdOwner;
    private Integer customerIdUser;
    private MobileServiceType serviceType;
    private Date serviceStartDate;


    public MobileNo(){
        //Default constructor
    }

    public MobileNo(final String msisdn, final Integer customerIdOwner, final  Integer customerIdUser, final MobileServiceType serviceType, final Date serviceStartDate) {
        this.msisdn = msisdn;
        this.customerIdOwner = customerIdOwner;
        this.customerIdUser = customerIdUser;
        this.serviceType = serviceType;
        this.serviceStartDate = serviceStartDate;
    }

    public void setId(final Integer id) {
        this.id = id;
    }


    public Integer getId() {
        return id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(final String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getCustomerIdOwner() {
        return customerIdOwner;
    }

    public void setCustomerIdOwner(final Integer customerIdOwner) {
        this.customerIdOwner = customerIdOwner;
    }

    public Integer getCustomerIdUser() {
        return customerIdUser;
    }

    public void setCustomerIdUser(final Integer customerIdUser) {
        this.customerIdUser = customerIdUser;
    }

    public MobileServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(final MobileServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Date getServiceStartDate() {
        return serviceStartDate;
    }

    public void setServiceStartDate(final Date serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
    }

}
