package com.codechallenge.api.mobileno.dao;

public enum MobileServiceType {
    MOBILE_PREPAID,
    MOBILE_POSTPAID
}
