package com.codechallenge.api.mobileno;


import com.codechallenge.api.mobileno.dao.MobileNo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MobileNoRepository extends JpaRepository<MobileNo, Integer> {
    MobileNo findByMsisdn(String msisdn);


}
