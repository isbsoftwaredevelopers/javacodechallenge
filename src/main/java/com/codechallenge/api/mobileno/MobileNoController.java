package com.codechallenge.api.mobileno;

import com.codechallenge.api.error.problemdetail.DuplicateEntity;
import com.codechallenge.api.error.problemdetail.EntityError;
import com.codechallenge.api.error.problemdetail.EntityNotFound;
import com.codechallenge.api.mobileno.dao.MobileNo;
import com.codechallenge.api.mobileno.dto.CreateMobileNoRequest;
import com.codechallenge.api.mobileno.dto.UpdateServiceTypeRequest;
import com.codechallenge.api.mobileno.dto.UpdateUserRequest;
import com.codechallenge.api.mobileno.dto.CreateMobileNoResponse;
import com.codechallenge.api.mobileno.dto.UpdateOwnerRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("mobile-no")
public class MobileNoController {

    private final IMobileNoService mobileNoService;

    public MobileNoController(final IMobileNoService mobileNoService) {
        this.mobileNoService = mobileNoService;
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<MobileNo>> getAll() {

        final var res = mobileNoService.getAll();
        return new ResponseEntity<>(res, HttpStatus.OK);
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<MobileNo> get(final @PathVariable("id") Integer id) {

        final var res = mobileNoService.get(id);
        if(res.isPresent()) {
            return new ResponseEntity<>(res.get(), HttpStatus.OK);
        }
        else {
            throw new EntityNotFound();
        }
    }

    @GetMapping("/search/{msisdn}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<MobileNo>> search(final @PathVariable("msisdn") String msisdn) {

        final var res = mobileNoService.search(msisdn);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @PutMapping("/changeServiceType/{id}")
    public ResponseEntity<Boolean> updateServiceType(final @PathVariable("id") Integer id,final @RequestBody UpdateServiceTypeRequest request) {
        final var res = mobileNoService.updateServiceType(id,request.getServiceType());
        if(res) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        }
        else {
            throw new EntityNotFound();
        }
    }

    @PutMapping("/changeOwner/{id}")
    public ResponseEntity<Boolean> updateOwner(final @PathVariable("id") Integer id,final @RequestBody UpdateOwnerRequest request) {
        final var res = mobileNoService.updateOwner(id,request.getUserId());
        if(res) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        }
        else {
            throw new EntityNotFound();
        }
    }


    @PutMapping("/changeUser/{id}")
    public ResponseEntity<Boolean> updateUser(final @PathVariable("id") Integer id,final @RequestBody UpdateUserRequest request) {
        final var res = mobileNoService.updateUser(id,request.getUserId());
        if(res) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        }
        else {
            throw new EntityNotFound();
        }
    }


    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CreateMobileNoResponse> create(final @RequestBody CreateMobileNoRequest mobileNo) {

        final var res = mobileNoService.create(mobileNo);
        if(res.isSuccess() && res.get().isPresent()) {
            return new ResponseEntity<>(new CreateMobileNoResponse(res.get().get()), HttpStatus.OK);
        }
        else if(res.isSuccess()) {
            throw new DuplicateEntity();
        }
        else {
            throw new EntityError(res.getCause());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Boolean> delete(final @PathVariable("id") Integer id) {
        final var res = mobileNoService.delete(id);
        if(res) {
            return new ResponseEntity<>(res, HttpStatus.OK);
        } else {
            throw new EntityNotFound();
        }
    }


}
