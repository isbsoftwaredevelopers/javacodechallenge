package com.codechallenge.api.mobileno;

import com.codechallenge.api.mobileno.dao.MobileNo;
import com.codechallenge.api.mobileno.dao.MobileServiceType;
import com.codechallenge.api.mobileno.dto.CreateMobileNoRequest;
import com.codechallenge.api.security.JwtTokenProviderService;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class MobileNoService implements IMobileNoService {

    private final MobileNoRepository repository;
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProviderService.class);

    public MobileNoService(final MobileNoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<MobileNo> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<MobileNo> get(final int id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Try<Optional<Integer>> create(final CreateMobileNoRequest mobileNo) {

        final PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        final var isValid = Try.of(() -> {
            try {
                 return phoneUtil.parse(mobileNo.getMsisdn(), "MT");
            } catch (NumberParseException ex) {
                final var errorMessage = String.format("Error parsing phone %s", mobileNo.getMsisdn());
                logger.error(errorMessage, ex);
                throw new RuntimeException(errorMessage,ex);
            }
        });

        if(isValid.isFailure()) {
            return Try.failure(isValid.getCause());
        }

        final var entityExists = repository.findByMsisdn(mobileNo.getMsisdn()) != null;
        if (entityExists) {
            return Try.of(Optional::empty);
        }

        final var res = repository.save(new MobileNo(mobileNo.getMsisdn(), mobileNo.getCustomerIdOwner(), mobileNo.getCustomerIdUser(), mobileNo.getServiceType(), mobileNo.getServiceStartDate()));
        return Try.of(()->Optional.of(res.getId()));

    }

    @Override
    public List<MobileNo> search(final String msisdn) {
        return repository.findAll().stream().filter(mobileNo -> mobileNo.getMsisdn().contains(msisdn)).collect(Collectors.toList());
    }

    @Override
    public Boolean delete(final Integer id) {
        try {
            repository.deleteById(id);
            return true;
        }
        catch (EmptyResultDataAccessException ex) {
            return false;
        }

    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Boolean updateServiceType(final Integer id,final  MobileServiceType serviceType) {
        return updateMobileNoProperty(id,(entity)->{
            entity.setServiceType(serviceType);
            return entity;
        });
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Boolean updateOwner(final Integer id,final  Integer ownerId) {
        return updateMobileNoProperty(id,(entity)->{
            entity.setCustomerIdOwner(ownerId);
            return entity;
        });
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Boolean updateUser(final Integer id,final  Integer userId) {
        return updateMobileNoProperty(id,(entity)->{
            entity.setCustomerIdUser(userId);
            return entity;
        });
    }

    private Boolean updateMobileNoProperty(final Integer id, final Function<MobileNo,MobileNo> modifier) {
        final var entity = repository.findById(id);
        if(entity.isEmpty()) {
            return false;
        }
        var mobileNo = entity.get();
        mobileNo = modifier.apply(mobileNo);
        repository.save(mobileNo);
        return true;
    }
}
