package com.codechallenge.api.mobileno;

import com.codechallenge.api.mobileno.dao.MobileNo;
import com.codechallenge.api.mobileno.dao.MobileServiceType;
import com.codechallenge.api.mobileno.dto.CreateMobileNoRequest;
import io.vavr.control.Try;

import java.util.List;
import java.util.Optional;

public interface IMobileNoService {
    List<MobileNo> getAll();
    Optional<MobileNo> get(int id);

    Try<Optional<Integer>> create(CreateMobileNoRequest mobileNo);

    List<MobileNo> search(String msisdn);

    Boolean delete(Integer id);

    Boolean updateServiceType(Integer id, MobileServiceType mobileNoType);

    Boolean updateOwner(Integer id, Integer ownerId);

    Boolean updateUser(Integer id, Integer userId);
}
