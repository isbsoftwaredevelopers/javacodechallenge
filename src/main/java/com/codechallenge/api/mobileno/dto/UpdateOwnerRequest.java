package com.codechallenge.api.mobileno.dto;

public class UpdateOwnerRequest {
    private Integer userId;

    public UpdateOwnerRequest() {
        // default constructor
    }

    public UpdateOwnerRequest(final Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(final Integer userId) {
        this.userId = userId;
    }
}
