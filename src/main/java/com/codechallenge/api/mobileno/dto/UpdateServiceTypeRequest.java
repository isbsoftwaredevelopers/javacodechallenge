package com.codechallenge.api.mobileno.dto;

import com.codechallenge.api.mobileno.dao.MobileServiceType;

public class UpdateServiceTypeRequest {
    private MobileServiceType serviceType;

    public UpdateServiceTypeRequest() {
        //default constructor
    }

    public UpdateServiceTypeRequest(final MobileServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public MobileServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(final MobileServiceType serviceType) {
        this.serviceType = serviceType;
    }
}
