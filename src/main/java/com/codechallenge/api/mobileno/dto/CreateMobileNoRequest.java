package com.codechallenge.api.mobileno.dto;

import com.codechallenge.api.mobileno.dao.MobileServiceType;

import java.util.Date;

public class CreateMobileNoRequest {
    private String msisdn;
    private Integer customerIdOwner;
    private Integer customerIdUser;
    private MobileServiceType serviceType;
    private Date serviceStartDate;

    public CreateMobileNoRequest() {
        //default constructor
    }

    public CreateMobileNoRequest(final String msisdn,final  Integer customerIdOwner,final  Integer customerIdUser, final MobileServiceType serviceType, final Date serviceStartDate) {
        this.msisdn = msisdn;
        this.customerIdOwner = customerIdOwner;
        this.customerIdUser = customerIdUser;
        this.serviceType = serviceType;
        this.serviceStartDate = serviceStartDate;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(final String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getCustomerIdOwner() {
        return customerIdOwner;
    }

    public void setCustomerIdOwner(final Integer customerIdOwner) {
        this.customerIdOwner = customerIdOwner;
    }

    public Integer getCustomerIdUser() {
        return customerIdUser;
    }

    public void setCustomerIdUser(final Integer customerIdUser) {
        this.customerIdUser = customerIdUser;
    }

    public MobileServiceType getServiceType() {
        return serviceType;
    }

    public void setServiceType(final MobileServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public Date getServiceStartDate() {
        return serviceStartDate;
    }

    public void setServiceStartDate(final Date serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
    }
}
