package com.codechallenge.api.mobileno.dto;

public class CreateMobileNoResponse {
    private final int id;

    public CreateMobileNoResponse(final int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

}
