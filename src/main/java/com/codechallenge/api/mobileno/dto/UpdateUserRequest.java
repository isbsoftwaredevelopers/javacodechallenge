package com.codechallenge.api.mobileno.dto;

public class UpdateUserRequest {
    private Integer userId;


    public UpdateUserRequest() {
        // default constructor
    }

    public UpdateUserRequest(final Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(final Integer userId) {
        this.userId = userId;
    }
}
